<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Model\Book;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Book::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'cover' => 'https://via.placeholder.com/600/92c952',
    ];
});
