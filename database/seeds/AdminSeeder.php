<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    public function run()
    {
        try {
            DB::table('users')->insert([
                'name' => 'dev',
                'email' => 'admin@dev.loc',
                'email_verified_at' => null,
                'password' => app()->make(\Illuminate\Hashing\HashManager::class)->make('aaa'),
                'remember_token' => str_random(40),
                'created_at' => null,
                'updated_at' => null
            ]);
        } catch (\Exception $e) {
            print $e;
        }

    }
}