<?php

use App\Model\Author;
use App\Model\Book;
use App\Model\Category;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);

        factory(Author::class, 5)->create();
        factory(Book::class, 5)->create();
        factory(Category::class, 5)->create();
    }
}
