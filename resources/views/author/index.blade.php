@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body books-container">
                        @if(auth()->check() == true)
                            <a href="{{ route('authors.create') }}" class="btn btn-success">Add author</a>
                        @endif
                        <div class="container">
                            <div class="row">
                                <ul>
                                @foreach($authors as $author)
                                    <li><a href="{{ route('authors.show', ['id' => $author->id]) }}">{{ $author->first_name }} {{ $author->last_name }}</a></li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection