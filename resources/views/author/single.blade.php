@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        @if(auth()->check() == true)
                            <a href="{{ route('authors.edit', ['id' => $author->id ]) }}" class="btn btn-success">Edit author</a>

                            <form role="form"
                                  action="{{ route('authors.destroy', ['author' => $author->id], false) }}"
                                  method="post" enctype="multipart/form-data">
                            <div>
                                @method('DELETE')
                                @csrf
                                <input type="submit" value="Delete" class="btn btn-danger">
                            </div>
                            </form>
                        @endif
                        <br> <br>
                        <div class="container">
                            <div class="row">

                                <div class="col-md-8">
                                    <p>{{ $author->first_name }} {{ $author->last_name }}</p>
                                    @if(count($author->books) > 0)
                                        Books:
                                    @endif
                                    @foreach($author->books as $book)
                                        <p><a href="{{ route('books.show', ['id' => $book->id]) }}">{{ $book->title }}</a></p>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection