@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <form role="form"
                              action="{{ route('authors.update', ['author' => $author->id], false) }}"
                              method="post" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf

                            <div class="form-group">
                                <input type="text" class="form-control" id="title" name="first_name" placeholder="First name" value="{{ $author->first_name }}" />
                                @if ($errors->has('first_name'))
                                    <div class="error">{{ $errors->first('first_name') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="title" name="last_name" placeholder="Last Name" value="{{ $author->last_name }}" />
                                @if ($errors->has('last_name'))
                                    <div class="error">{{ $errors->first('last_name') }}</div>
                                @endif
                            </div>

                            <input type="submit" value="Update" class="btn btn-primary">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

