@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <form role="form"
                              action="{{ route('category.store', [], false) }}"
                              method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <input type="text" class="form-control" id="title" name="name" placeholder="Name" value="" />
                                @if ($errors->has('name'))
                                    <div class="error">{{ $errors->first('name') }}</div>
                                @endif
                            </div>

                            <input type="submit" value="Save" class="btn btn-primary">

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection