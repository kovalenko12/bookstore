@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body books-container">
                        @if(auth()->check() == true)
                            <a href="{{ route('category.create') }}" class="btn btn-success">Add category</a>
                        @endif
                        <div class="container">
                            <div class="row">
                                <ul>
                                    @foreach($categories as $category)
                                        <li><a href="{{ route('category.show', ['id' => $category->id]) }}">{{ $category->name }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection