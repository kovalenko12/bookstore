@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <form role="form"
                              action="{{ route('books.update', ['book' => $book->id], false) }}"
                              method="post" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ $book->title }}" />
                                @if ($errors->has('title'))
                                    <div class="error">{{ $errors->first('title') }}</div>
                                @endif
                            </div>
                            @if(!empty($book->cover))
                                <img src="{{ asset($book->cover) }}" alt="" style="max-width: 150px">
                            @endif

                            <div class="form-group">
                                <br>
                                <input type="file" class="filestyle" id="cover" name="cover">
                                @if ($errors->has('cover'))
                                    <div class="error">{{ $errors->first('cover') }}</div>
                                @endif
                            </div>

                            <select multiple id="authors" style="width:100%" name="authors[]">
                                @foreach($authors as $author)
                                    <option value="{{$author->id}}"
                                            {{ $book->isSelectedAuthor($author->id) }}>{{$author->first_name}} {{$author->last_name}}</option>
                                @endforeach
                                {{-- @if(old('status', $model->status) === $status) selected @endif --}}
                            </select>

                            <select multiple id="categories" style="width:100%" name="categories[]">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" {{ $book->isSelectedCategory($category->id) }}>{{$category->name}}</option>
                                @endforeach
                            </select>

                            <br> <br>
                            <input type="submit" value="Update">

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('headerScripts')
    <style>
        .select2-container--default .select2-selection--single {
            display: block;
            border-radius: 0;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        }
    </style>
@endsection

@section('footerScripts')
<script>
    $(document).ready(function () {
        var $authors = $('#authors');
        $authors.select2({
            tags: true,
        });

        $authors.on("select2:select", function (e) {
            var $electedTags = $('#selectedTags');
            var curVal = $electedTags.val();
            curVal = prepareSelect2(curVal);
            var item = {
                "id": e.params.data.id === e.params.data.text ? null : e.params.data.id,
                "select2Id": e.params.data.id,
                "name": e.params.data.text
            };
            curVal.push(item);
            $electedTags.val(JSON.stringify(curVal));
        });
        $authors.on("select2:unselect", function (e) {
            var $electedTags = $('#selectedTags');
            var curVal = $electedTags.val();
            curVal = prepareSelect2(curVal);
            curVal = curVal.filter(function (item) {
                return item['select2Id'] !== e.params.data.id;
            });
            $electedTags.val(JSON.stringify(curVal));
        });

        var prepareSelect2 = function (value) {
            if (value) {
                value = JSON.parse(value);
            }
            if (!Array.isArray(value)) {
                value = value ? new Array(value) : [];
            }

            return value;
        }
    });
</script>
@endsection

