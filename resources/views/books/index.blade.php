@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body books-container">
                        @if(auth()->check() == true)
                            <a href="{{ route('books.create') }}" class="btn btn-success">Add book</a>
                        @endif
                        <div class="container">
                            <div class="row">
                                @foreach($books as $book)
                                    <a class="col-md-3" href="{{ route('books.show', ['id' => $book->id]) }}">
                                        <p>{{ $book->title }}</p>
                                        <img src="{{ $book->cover }}" alt="">
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection