@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        @if(auth()->check() == true)
                            <a href="{{ route('books.edit', ['id' => $book->id ]) }}" class="btn btn-success">Edit book</a>

                            <form role="form"
                                  action="{{ route('books.destroy', ['id' => $book->id], false) }}"
                                  method="post" enctype="multipart/form-data">
                                <div>
                                    @method('DELETE')
                                    @csrf
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                </div>
                            </form>
                        @endif
                        <br> <br>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{ asset($book->cover) }}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <p>{{ $book->title }}</p>
                                    <p>
                                        @if(count($book->authors) == 1)
                                            Author:
                                        @elseif(count($book->authors) > 1)
                                            Authors:
                                        @endif

                                        @foreach($book->authors as $author)
                                            <span>{{ $author->first_name }} {{ $author->last_name }}</span>,
                                            @if (!$loop->last),@endif
                                        @endforeach
                                    </p>

                                    <p>
                                        @if(count($book->categories) == 1)
                                            Category:
                                        @elseif(count($book->categories) > 1)
                                            Categories:
                                        @endif

                                        @foreach($book->categories as $category)
                                            <span>{{ $category->name }} </span>
                                            @if (!$loop->last),@endif
                                        @endforeach
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection