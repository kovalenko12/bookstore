@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <form role="form"
                              action="{{ route('books.store', [], false) }}"
                              method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="" />
                                @if ($errors->has('title'))
                                    <div class="error">{{ $errors->first('title') }}</div>
                                @endif
                            </div>

                            <div class="form-grop">
                                <input type="file" class="filestyle" id="cover" name="cover">
                                @if ($errors->has('cover'))
                                    <div class="error">{{ $errors->first('cover') }}</div>
                                @endif
                            </div>

                            <select multiple id="authors" style="width:100%" name="authors[]">
                                @foreach($authors as $author)
                                    <option value="{{$author->id}}"
                                            >{{$author->first_name}} {{$author->last_name}}</option>
                                @endforeach
                            </select>
                            {{--<input id="selectedTags" type="hidden" name="tags" value="">--}}

                            <select multiple id="categories" style="width:100%" name="categories[]">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>

                            <br><br>
                            <input type="submit" value="Save">

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection