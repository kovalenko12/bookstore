<?php

namespace App\Repositories;

use App\Model\Book;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use App\Interfaces\BookRepositoryInterface;
use Illuminate\Http\UploadedFile;
use Carbon\Carbon;

class BookRepository implements BookRepositoryInterface
{
    /**
     * @var Book
     */
    private $model;

    /**
     * @var string
     */
    private $storageBasePath = 'storage';
    private $basePath = 'covers';

    public function __construct(Book $book)
    {
        $this->model = $book;
    }

    public function all($attributes = ['*']): Collection
    {
        return $this->model->get($attributes);
    }

    public function getOne(int $id): Book
    {
        $book = $this->model->where('id', $id)->first();

        if (!($book instanceof Book)) {
            throw new ModelNotFoundException('Book not found!');
        }

        return $book;
    }

    public function store(array $attributes = []): Book
    {
        if (isset($attributes['cover']) && !empty($attributes['cover'])) {
            $coverPath = $this->getPath($attributes['cover']);
            $isSaved = \Storage::put($coverPath, file_get_contents($attributes['cover']->getRealPath()));
            if ($isSaved) {
                $attributes['cover'] = $this->storageBasePath . '/' . $coverPath;
            }
        }

        $book = $this->model->create($attributes);
        $book->authors()->sync($attributes['authors']);
        $book->categories()->sync($attributes['categories']);

        return $book;
    }

    public function update(Book $book, array $attributes): Book
    {
        if (isset($attributes['cover']) && !empty($attributes['cover'])) {
            $coverPath = $this->getPath($attributes['cover']);
            $isSaved = \Storage::put($coverPath, file_get_contents($attributes['cover']->getRealPath()));
            if ($isSaved) {
                $attributes['cover'] = $this->storageBasePath . '/' . $coverPath;
            }
        }

        $book->update($attributes);

        return $book;
    }

    public function getEmptyModel(): Book
    {
        return $this->model->newInstance();
    }

    public function delete(int $id)
    {
        return $this->model->where('id', $id)->first()->delete();
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function getPath(UploadedFile $file): string
    {
        return $this->basePath . '/' . $this->generateName($file);
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function generateName(UploadedFile $file): string
    {
        return $this->getNowAsTimestamp() . '.' . $file->clientExtension();
    }

    private function getNowAsTimestamp(): int
    {
        return Carbon::now()->timestamp;
    }
}