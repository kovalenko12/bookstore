<?php

namespace App\Repositories;

use App\Interfaces\AuthorRepositoryInterface;
use App\Model\Author;
use Illuminate\Support\Collection;

class AuthorRepository implements AuthorRepositoryInterface
{

    private $model;

    public function __construct(Author $author)
    {
        $this->model = $author;
    }

    public function all($attributes = ['*']): Collection
    {
        return $this->model->get($attributes);
    }

    public function getOne(int $id): Author
    {
        $author = $this->model->where('id', $id)->first();

        if (!($author instanceof Author)) {
            throw new ModelNotFoundException('Book not found!');
        }

        return $author;
    }

    public function getEmptyModel(): Author
    {
        return $this->model->newInstance();
    }

    public function store(array $attributes): Author
    {
        $author = $this->model->create($attributes);

        return $author;
    }

    public function update(Author $author, array $attributes): Author
    {
        $author->update($attributes);

        return $author;
    }

    public function delete(int $id)
    {
        return $this->model->where('id', $id)->first()->delete();
    }
}