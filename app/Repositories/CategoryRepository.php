<?php

namespace App\Repositories;

use App\Interfaces\CategoryRepositoryInterface;
use App\Model\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

class CategoryRepository implements CategoryRepositoryInterface
{

    private $model;

    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    public function all($attributes = ['*']): Collection
    {
        return $this->model->get($attributes);
    }

    public function getOne(int $id): Category
    {
        $category = $this->model->where('id', $id)->first();

        if (!($category instanceof Category)) {
            throw new ModelNotFoundException('Category not found!');
        }

        return $category;
    }

    public function getEmptyModel(): Category
    {
        return $this->model->newInstance();
    }

    public function store(array $attributes): Category
    {
        $category = $this->model->create($attributes);

        return $category;
    }

    public function update(Category $category, array $attributes): Category
    {
        $category->update($attributes);

        return $category;
    }

    public function delete(int $id)
    {
        return $this->model->where('id', $id)->first()->delete();
    }
}