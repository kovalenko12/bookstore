<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthorRequest;
use App\Interfaces\AuthorRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class AuthorController
 * @package App\Http\Controllers
 */
class AuthorController extends Controller
{

    /**
     * @var AuthorRepositoryInterface
     */
    private $repository;

    /**
     * AuthorController constructor.
     * @param AuthorRepositoryInterface $authorRepository
     */
    public function __construct(AuthorRepositoryInterface $authorRepository)
    {
        $this->repository = $authorRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authors = $this->repository->all();
        return view('author.index', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $author = $this->repository->getEmptyModel();

        return view('author.create', compact('author'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param AuthorRequest $request
     */
    public function store(AuthorRequest $request)
    {
        $this->repository->store($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $author = $this->repository->getOne($id);
        return view('author.single', compact('author'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author = $this->repository->getOne($id);

        return view('author.edit', compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AuthorRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AuthorRequest $request, $id)
    {
        try {
            $author = $this->repository->getOne($id);
            $this->repository->update($author, $request->all());
        } catch (ModelNotFoundException $e) {
            return redirect('authors');
        }

        return redirect('authors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
        } catch (ModelNotFoundException $e) {
            return redirect('authors');
        }

        return redirect('authors');
    }
}
