<?php

namespace App\Http\Controllers;

use App\Model\Author;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Interfaces\BookRepositoryInterface;
use App\Model\Book;
use App\Http\Requests\BookRequest;
use Illuminate\Support\Collection;

/**
 * Class BooksController
 * @package App\Http\Controllers
 */
class BookController extends Controller
{
    /**
     * @var BookRepositoryInterface
     */
    private $repository;

    /**
     * BooksController constructor.
     * @param BookRepositoryInterface $bookRepository
     */
    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->repository = $bookRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = $this->repository->all();
        return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $book = $this->repository->getEmptyModel();
        $authors = \App\Model\Author::all();
        $categories = \App\Model\Category::all();
        return view('books.create', compact('book', 'authors', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $this->repository->store($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = $this->repository->getOne($id);
        return view('books.single', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = $this->repository->getOne($id);
        $authors = \App\Model\Author::all();
        $categories = \App\Model\Category::all();
        return view('books.edit', compact('book', 'authors', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, BookRequest $request)
    {
        try {
            $book = $this->repository->getOne($id);
            $this->repository->update($book, $request->all());
        } catch (ModelNotFoundException $e) {
            return redirect('books');
        }

        return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
        } catch (ModelNotFoundException $e) {
            return redirect('books');
        }

        return redirect('books');
    }
}
