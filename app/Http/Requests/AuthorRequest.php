<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthorRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'first_name' => [
                'required',
                'max:191',
                'regex:/(^[a-zA-Z0-9~@#$^*()_+[\]{}|,.?: -]*$)/u'
            ],

            'last_name' => [
                'required',
                'max:191',
                'regex:/(^[a-zA-Z0-9~@#$^*()_+[\]{}|,.?: -]*$)/u'
            ],
        ];
    }
}