<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'title' => [
                'required',
                'max:191',
                'regex:/(^[a-zA-Z0-9~@#$^*()_+[\]{}|,.?: -]*$)/u'
            ],
            'cover' => [
                'file',
                'image',
                'max:5120' // 5 mb
            ],
        ];
    }
}