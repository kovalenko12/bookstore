<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:191',
                'regex:/(^[a-zA-Z0-9~@#$^*()_+[\]{}|,.?: -]*$)/u'
            ],
        ];
    }
}