<?php

namespace App\Interfaces;

use App\Model\Category;
use Illuminate\Support\Collection;

/**
 * Interface BookRepositoryInterface
 * @package App\Interfaces
 */
interface CategoryRepositoryInterface
{
    /**
     * @param array $attributes
     * @return Collection
     */
    public function all($attributes = ['*']): Collection;

    /**
     * @param int $id
     * @return Category
     */
    public function getOne(int $id): Category;

    /**
     * @return Category
     */
    public function getEmptyModel(): Category;

    /**
     * @param array $attributes
     * @return Category
     */
    public function store(array $attributes): Category;

    /**
     * @param Category $category
     * @param array $attributes
     * @return Category
     */
    public function update(Category $category, array $attributes): Category;

    /**
     * @param int $id
     * @return mixed
     */
    public function delete(int $id);



}