<?php

namespace App\Interfaces;

use App\Model\Book;
use Illuminate\Support\Collection;

/**
 * Interface BookRepositoryInterface
 * @package App\Interfaces
 */
interface BookRepositoryInterface
{
    /**
     * @param array $attributes
     * @return Collection
     */
    public function all($attributes = ['*']): Collection;

    /**
     * @param int $id
     * @return Book
     */
    public function getOne(int $id): Book;

    /**
     * @param array $attributes
     * @return Book
     */
    public function store(array $attributes = []): Book;

    /**
     * @param Book $book
     * @param array $attributes
     * @return Book
     */
    public function update(Book $book, array $attributes): Book;

    /**
     * @return Book
     */
    public function getEmptyModel(): Book;

    /**
     * @param int $id
     * @return mixed
     */
    public function delete(int $id);

}