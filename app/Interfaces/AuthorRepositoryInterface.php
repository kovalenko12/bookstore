<?php

namespace App\Interfaces;

use App\Model\Author;
use App\Model\Category;
use Illuminate\Support\Collection;

/**
 * Interface BookRepositoryInterface
 * @package App\Interfaces
 */
interface AuthorRepositoryInterface
{
    /**
     * @param array $attributes
     * @return Collection
     */
    public function all($attributes = ['*']): Collection;

    /**
     * @param int $id
     * @return Author
     */
    public function getOne(int $id): Author;

    /**
     * @return Author
     */
    public function getEmptyModel(): Author;

    /**
     * @param array $attributes
     * @return Author
     */
    public function store(array $attributes): Author;

    /**
     * @param int $id
     * @return mixed
     */
    public function delete(int $id);



}