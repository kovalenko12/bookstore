<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Book extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'cover',
    ];

    public function authors(): BelongsToMany
    {
        return $this->belongsToMany(Author::class, 'book_authors');
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'book_categories');
    }

    function isSelectedAuthor($id) {
        if(!($ids = old('author'))) {
            $ids = $this->categories->pluck('id');
        }

        return collect($ids)->contains($id) ? 'selected' : '';
    }

    function isSelectedCategory($id) {

        if(!($ids = old('category'))) {
            $ids = $this->categories->pluck('id');
        }

        return collect($ids)->contains($id) ? 'selected' : '';
    }
}
