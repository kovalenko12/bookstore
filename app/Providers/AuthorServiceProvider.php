<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AuthorServiceProvider extends ServiceProvider
{
    private $dependencies = [
        [
            'App\Interfaces\AuthorRepositoryInterface',
            'App\Repositories\AuthorRepository'
        ],
    ];

    public function register()
    {
        foreach ($this->dependencies as $dependency) {
            $this->app->bind($dependency[0], $dependency[1]);
        }
    }
}