<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CategoryServiceProvider extends ServiceProvider
{
    private $dependencies = [
        [
            'App\Interfaces\CategoryRepositoryInterface',
            'App\Repositories\CategoryRepository'
        ],
    ];

    public function register()
    {
        foreach ($this->dependencies as $dependency) {
            $this->app->bind($dependency[0], $dependency[1]);
        }
    }
}