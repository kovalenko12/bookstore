<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BookServiceProvider extends ServiceProvider
{
    private $dependencies = [
        [
            'App\Interfaces\BookRepositoryInterface',
            'App\Repositories\BookRepository'
        ],
    ];

    public function register()
    {
        foreach ($this->dependencies as $dependency) {
            $this->app->bind($dependency[0], $dependency[1]);
        }
    }
}