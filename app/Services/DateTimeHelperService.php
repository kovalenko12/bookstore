<?php

namespace App\Services\DateTime;

use Carbon\Carbon;

class DateTimeHelperService
{
    /**
     * @param string $dates
     * @return Carbon[]
     */
    public function parseDateRange(string $dates): array
    {
        $arrayDates = explode('-', $dates);

        return [
            'start' => Carbon::createFromTimeString(trim($arrayDates[0])),
            'end' => Carbon::createFromTimeString(trim($arrayDates[1])),
        ];
    }

    /**
     * @return Carbon
     */
    public function getNow(): Carbon
    {
        return Carbon::now();
    }

    /**
     * @return int
     */
    public function getNowAsTimestamp(): int
    {
        return Carbon::now()->timestamp;
    }
}